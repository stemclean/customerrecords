## Requirements

NodeJS v7 (developed using v7.10.0)


## Installation & Usage

1. Run `npm install` from project root.
2. Run `npm run build` from project root.
3. Run `node dist/app.js` from project root. You can also add additional arguments to this call in the form: `node dist/app.js LATITUDE LONGITUDE DISTANCE`
4. Run `npm test` to view unit test results. 
