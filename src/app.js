const fs = require('fs');
const path = require('path');

import DistanceCalculator from './distancecalculator';

const readFileByLine = (filePath) => {
  if (fs.existsSync(filePath)) {
    const file = fs.readFileSync(filePath, {encoding: 'utf-8'});
    const lines = file.toString().split('\n');

    // Split will return an extra array element '' - remove it here.
    return lines.filter(line => !!line);
  }

  throw new Error(`File does not exist ${filePath}`);
};

/**
 * Turn an array of JSON strings read from disk, into a usable
 * array of JSON parsed objects.
 */
const parseRawArray = (array) => {
  array = array || [];
  return array.map(item => {
      const parsedItem = JSON.parse(item);

      if (!parsedItem) {
        throw new Error(`Invalid JSON provided ${item}`);
      }

      return parsedItem;
  });
};

const shouldInvite = (customer, latitude, longitude, acceptableDistance) => {
  const customerDistanceFromPoint =
    DistanceCalculator.calculateDistance(customer.latitude,
                                          customer.longitude,
                                          latitude,
                                          longitude);
  return customerDistanceFromPoint <= acceptableDistance;
};

const getInvitableCustomers = (filePath, latitude=53.339428, longitude=-6.257664, distance=100) => {

  try {
    const unparsedCustomers = readFileByLine(filePath);
    const customers = parseRawArray(unparsedCustomers);

    return customers.filter(customer => {
      return shouldInvite(customer, latitude, longitude, distance);
    });
  }
  catch(error) {
    throw new Error(`Unable to get customer invites because of ${error}`);
  }
};

// Only run if called directly, not from unit tests.
if (require.main === module) {

  let latitude, longitude, distance;

  if (process.argv.length >= 3) {
    latitude = process.argv[2];
  }

  if (process.argv.length >= 4) {
    longitude = process.argv[3];
  }

  if (process.argv.length >= 5) {
    distance = process.argv[4];
  }

  const invitableCustomers = getInvitableCustomers(
                              path.join(__dirname, '../customers.json'),
                              latitude,
                              longitude,
                              distance);

  invitableCustomers.sort((first, second) => {
    return first.user_id - second.user_id;
  })
  .forEach(customer => {
    console.log(`${customer.name} ${customer.user_id}`);
  });
}

export { readFileByLine, parseRawArray, shouldInvite, getInvitableCustomers };
