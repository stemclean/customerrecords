// Approximite radius of Earth in km
const RADIUS = 6371;

class DistanceCalculator {

  static toRadians(angle) {
    return angle * (Math.PI / 180);
  }

  static calculateDistance(lat1, lon1, lat2, lon2) {
    const latitudeDiff = this.toRadians(lat1 - lat2);
    const longitudeDiff = this.toRadians(lon1 - lon2);
    const lat1Radians = this.toRadians(lat1);
    const lat2Radians = this.toRadians(lat2);

    const a = Math.sin(latitudeDiff / 2) * Math.sin(longitudeDiff / 2) +
            Math.cos(lat1Radians) * Math.cos(lat2Radians) *
            Math.sin(longitudeDiff / 2) * Math.sin(longitudeDiff / 2);
    const centalAngle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return RADIUS * centalAngle;
  }

}

export default DistanceCalculator;
