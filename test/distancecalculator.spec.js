const expect = require('chai').expect;

import DistanceCalculator from '../src/distancecalculator';

describe('DistanceCalculator', () => {

  describe('toRadians', () => {
    it('Should correctly convert angles to radians', () => {
      expect(DistanceCalculator.toRadians(45)).to.equal(0.7853981633974483);
    });
  });

  describe('calculateDistance', () => {
    it('Should correctly calculate the distance between coordinates', () => {
      const lat1 = 53.339428;
      const lon1 = -6.257664;
      const lat2 = 53.0033946;
      const lon2 = -6.3877505;

      expect(DistanceCalculator.calculateDistance(lat1, lon1, lat2, lon2)).to.equal(24.812579320975022); 
    });
  });

});
