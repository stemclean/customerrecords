const expect = require('chai').expect;
const path = require('path');
const _ = require('underscore');

import { readFileByLine, parseRawArray, shouldInvite } from '../src/app';

describe('readFileByLine', () => {
  it('Should return the correct number of lines for a valid file', () => {
    const filePath = path.join(__dirname, './spec/customers.json');
    const lines = readFileByLine(filePath);

    expect(lines).to.exist;
    expect(lines.length).to.equal(32);
  });

  it('Should throw a descriptive error if a file does not exist', () => {
    const filePath = path.join(__dirname, './spec/does_not_exist.json');
    expect(() => readFileByLine(filePath)).to.throw(`File does not exist ${filePath}`);
  });
});

describe('parseRawArray', () => {
  it('Should return an array of objects', () => {
    const testData = ['{"test" : "test"}'];
    const parsedData = parseRawArray(testData);

    expect(parsedData).to.exist;
    expect(parsedData).to.be.an('array');
    parsedData.forEach(parsedElement => {
      expect(parsedElement).to.be.an('object')
    });

  });

  it('Should return correctly parsed objects', () => {
    const testData = ['{"test" : "test"}'];
    const parsedData = parseRawArray(testData);

    expect(parsedData).to.exist;
    expect(parsedData).to.be.an('array');
    expect(parsedData.length).to.equal(1);

    const expectedParsedObject = {"test" : "test"};
    const actualParsedObject = parsedData[0];
    expect(_.isEqual(expectedParsedObject, actualParsedObject)).to.be.true;
  });

  it('Should throw an error if invalid JSON is provided', () => {
    expect(() => parseRawArray(['{'])).to.throw();

    const nullItem = ['null']
    expect(() => parseRawArray(nullItem)).to.throw(`Invalid JSON provided ${nullItem[0]}`);
  });

  it('Should not fail for null or falsy input', () => {
    expect(() => parseRawArray(null)).to.not.throw();
    expect(() => parseRawArray(undefined)).to.not.throw();
    expect(() => parseRawArray(0)).to.not.throw();
    expect(() => parseRawArray('')).to.not.throw();
  });

  it('Should return an empty array, even given null or falsy input', () => {
    const result = parseRawArray(null);
    expect(result).to.exist;
    expect(result).to.be.an('array');
  });

});

describe('shouldInvite', () => {
  it('Should invite customers within the distance', () => {
    const validCustomer = {
      latitude: 53.1229599,
      longitude: -6.2705202
    };

    const latitude = 53.339428;
    const longitude = -6.257664;
    const distance = 100;

    expect(shouldInvite(validCustomer, latitude, longitude, distance)).to.be.true;
  });

  it('Should not invite customers outside the distance', () => {
    const invalidCustomer = {
      latitude: 51.92893,
      longitude: -10.27699
    };

    const latitude = 53.339428;
    const longitude = -6.257664;
    const distance = 100;

    expect(shouldInvite(invalidCustomer, latitude, longitude, distance)).to.be.false;   
  });
});
